BROWSER ?= firefox
BUILD_DIR = build
NET_DIR = assets
elm19 = ~/.local/bin/elm

# --- Goals ---
.DEFAULT_GOAL = debug
.PHONY: clean debug release


# --- Static content ---
CONTENT = $(patsubst $(NET_DIR)/%,$(BUILD_DIR)/%,$(shell find $(NET_DIR) -type f))

# --- Javascript content ---
ELM_SOURCE = $(shell find front -type f)

# copy assets into the server filesystem
$(CONTENT) : $(BUILD_DIR)/% : $(NET_DIR)/%
	(cd $(NET_DIR) && cp --parents $(patsubst $(NET_DIR)/%,%,$<) ../$(BUILD_DIR))

debug : $(ELM_SOURCE) $(CONTENT)
	$(elm19) make --debug --output=$(BUILD_DIR)/p/react.js front/React.elm
	$(elm19) make --debug --output=$(BUILD_DIR)/p/presentation.js front/Presentation.elm
	$(elm19) make --debug --output=$(BUILD_DIR)/p/control.js front/Control.elm

release : $(ELM_SOURCE) $(CONTENT)
	$(elm19) make --optimize --output=$(BUILD_DIR)/p/react.js front/React.elm
	$(elm19) make --optimize --output=$(BUILD_DIR)/p/presentation.js front/Presentation.elm
	$(elm19) make --optimize --output=$(BUILD_DIR)/p/control.js front/Control.elm
	uglifyjs $(BUILD_DIR)/p/presentation.js \
		--compress 'pure_funcs="F2,F3,F4,F5,F6,F7,F8,F9,A2,A3,A4,A5,A6,A7,A8,A9",pure_getters=true,keep_fargs=false,unsafe_comps=true,unsafe=true,passes=2' \
		--output=$(BUILD_DIR)/p/presentation.js \
		&& uglifyjs $(BUILD_DIR)/p/presentation.js --mangle --output=$(BUILD_DIR)/p/presentation.js
	uglifyjs $(BUILD_DIR)/p/react.js \
		--compress 'pure_funcs="F2,F3,F4,F5,F6,F7,F8,F9,A2,A3,A4,A5,A6,A7,A8,A9",pure_getters=true,keep_fargs=false,unsafe_comps=true,unsafe=true,passes=2' \
		--output=$(BUILD_DIR)/p/react.js \
		&& uglifyjs $(BUILD_DIR)/p/react.js --mangle --output=$(BUILD_DIR)/p/react.js
	uglifyjs $(BUILD_DIR)/p/control.js \
		--compress 'pure_funcs="F2,F3,F4,F5,F6,F7,F8,F9,A2,A3,A4,A5,A6,A7,A8,A9",pure_getters=true,keep_fargs=false,unsafe_comps=true,unsafe=true,passes=2' \
		--output=$(BUILD_DIR)/p/control.js \
		&& uglifyjs $(BUILD_DIR)/p/control.js --mangle --output=$(BUILD_DIR)/p/control.js

clean:
	rm -rf $(BUILD_DIR)/*
