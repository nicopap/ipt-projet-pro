# Projet Professionnel

This repository holds the source code for the "Présentation du Projet
Professionnel" that I have done the 13th of May in the setting of the
*Jeunes@Work* cursus.

Pictures (typically logos and icons) are purposely left out, as -- even if
it is fair use -- I don't hold the copyright on most of them.

## Building

To build the frontend, one needs the elm 0.19 compiler. For optimisation,
uglifyjs is also needed (release build). A Makefile is provided to automatise
the process.

The backend is a simple python script. One may use pip to install the
python dependencies with the `pip -r requirements.txt` command. One **should**
use `virtualenv` to have a local python install.

## License

(c) Nicola Papale

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
