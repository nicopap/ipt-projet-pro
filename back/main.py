#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

import aiohttp
from aiohttp import web

listenersConnections = []
presentationConnections = []

async def websocket_receive_handler(request):
    ws = web.WebSocketResponse()
    await ws.prepare(request)
    listenersConnections.append(ws)
    async for msg in ws:
        if msg.type == aiohttp.WSMsgType.TEXT:
            if msg.data == 'close':
                await ws.close()
        elif msg.type == aiohttp.WSMsgType.ERROR:
            print(f'wd connection closed with exception {ws.exception()}')
    print('websocket connection closed')
    listenersConnections.remove(ws)
    return ws

async def websocket_control_send(request):
    ws = web.WebSocketResponse()
    await ws.prepare(request)
    presentationConnections.append(ws)
    async for msg in ws:
        if msg.type == aiohttp.WSMsgType.TEXT:
            if msg.data == 'close':
                await ws.close()
        elif msg.type == aiohttp.WSMsgType.ERROR:
            print(f'wd connection closed with exception {ws.exception()}')
    print('websocket connection closed')
    presentationConnections.remove(ws)
    return ws

async def websocket_control_receive(request):
    ws = web.WebSocketResponse()
    await ws.prepare(request)
    async for msg in ws:
        if msg.type == aiohttp.WSMsgType.TEXT:
            if msg.data == 'close':
                await ws.close()
            else:
                cmdId = msg.data
                if cmdId in 'PSN':
                    print(cmdId)
                    for conn in presentationConnections:
                        await conn.send_str(cmdId)
        elif msg.type == aiohttp.WSMsgType.ERROR:
            print(f'wd connection closed with exception {ws.exception()}')
    print('websocket connection closed')
    return ws

async def websocket_send_handler(request):
    ws = web.WebSocketResponse()
    await ws.prepare(request)
    async for msg in ws:
        if msg.type == aiohttp.WSMsgType.TEXT:
            if msg.data == 'close':
                await ws.close()
            else:
                emojiId = msg.data
                if emojiId == '1':
                    emoji = '\U0001f44c' #ok hand sign
                elif emojiId == '2':
                    emoji = '\U0001f4af' #hundred points symbol
                elif emojiId == '3':
                    emoji = '\U0001f44f' #clapping hands sign
                elif emojiId == '4':
                    emoji = '\U0001f35d' #spaghetti
                elif emojiId == '5':
                    emoji = '\U0001f35c' #steaming bowl
                elif emojiId == '6':
                    emoji = '\U0001f956' #baguette bread
                else:
                    emoji = None

                if emoji is not None:
                    print(emoji)
                    for conn in listenersConnections:
                        await conn.send_str(emoji)
        elif msg.type == aiohttp.WSMsgType.ERROR:
            print(f'wd connection closed with exception {ws.exception()}')
    print('websocket connection closed')
    return ws

app = web.Application()
app.router.add_route('*', '/p/receivews', websocket_receive_handler)
app.router.add_route('*', '/p/sendws', websocket_send_handler)
web.run_app(app, port=8011)
