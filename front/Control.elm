import Dict exposing (Dict)
import Browser
import Html exposing (Html, text, div, button)
import Html.Attributes as Attr exposing (class)
import Html.Events exposing (onClick)
import Json.Encode as Enc
import Ports


main : Program () Model Msg
main = Browser.element
    { init = always ((), Cmd.none)
    , update = update
    , view = view
    , subscriptions = always Sub.none
    }

type alias Model = ()

type Msg = SendCmd String


update : Msg -> Model -> (Model, Cmd Msg)
update msg () =
    case msg of
        SendCmd which ->
            ((), Ports.wssend which)


emojiButton : String -> Html Msg
emojiButton unicodeChar =
    button [ onClick <| SendCmd unicodeChar ] [ text unicodeChar ]

view : Model -> Html Msg
view () =
    div [class "emoji-palette"]
        [ emojiButton "P"
        , emojiButton "S"
        , emojiButton "N"
        ]
