import Dict exposing (Dict)
import Browser
import Html exposing (Html, text, div, button)
import Html.Attributes as Attr exposing (class)
import Html.Events exposing (onClick)
import Json.Encode as Enc
import Emoji exposing (replaceWithTwemoji)
import Ports
import LiveEmojis exposing (LiveEmojis)


main : Program () Model Msg
main = Browser.element
    { init = always (init, Cmd.none)
    , update = update
    , view = view
    , subscriptions = always <| Sub.map LiveEmojisMsg LiveEmojis.sub
    }

type alias Model =
    { live : LiveEmojis
    }

type Msg
    = SendEmoji Int
    | LiveEmojisMsg LiveEmojis.Msg


init : Model
init =
    { live = LiveEmojis.init
    }

update : Msg -> Model -> (Model, Cmd Msg)
update msg { live } =
    case msg of
        SendEmoji which ->
            ({ live = live }, Ports.wssend <| String.fromInt which)
        LiveEmojisMsg msg_ ->
            let (newLive, cmds) = LiveEmojis.update msg_ live
            in ({ live = newLive }, Cmd.map LiveEmojisMsg cmds)


emojiButton : Int -> String -> Html Msg
emojiButton index unicodeChar =
    button
        [ onClick <| SendEmoji index ]
        (Emoji.textWith replaceWithTwemoji unicodeChar)

view : Model -> Html Msg
view { live } =
    div []
        [ div [class "emoji-palette"]
            [ emojiButton 1 "\u{1f44c}"
            , emojiButton 2 "\u{1f4af}"
            , emojiButton 3 "\u{1f44f}"
            , emojiButton 4 "\u{1f35d}"
            , emojiButton 5 "\u{1f35c}"
            , emojiButton 6 "\u{1f956}"
            ]
        , Html.map LiveEmojisMsg <| LiveEmojis.view live
        ]
