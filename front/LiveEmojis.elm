module LiveEmojis exposing (Msg, init, LiveEmojis, update, view, sub, counts)

import Process exposing (sleep)
import Task
import Html exposing (Html)
import Html.Attributes exposing (class, classList, style)
import Html.Keyed as HK
import Array exposing (Array, length, push, set)
import Emoji exposing (replaceWithTwemoji)
import Json.Decode as Decode exposing (Decoder, decodeValue, errorToString)
import Json.Encode as Encode exposing (Value)
import Ports
import Dict exposing (Dict)



type LiveEmojis
    = LiveEmojis
        { buffered : Array Emoji
        , nextFree : Int
        , emojiCount : Dict String Int
        }

type Visibility
    = Init
    | Anim
    | Dead

type alias Emoji =
    { emoji: String
    , visible: Visibility
    , offset : Int
    }

type Msg
    = AddEmoji String Int
    | AnimateEmoji Int
    | HideEmoji Int
    | EmojiError String


timeout : Float -> Msg -> Cmd Msg
timeout miliseconds msg =
    sleep miliseconds
        |> Task.andThen (\() -> Task.succeed msg)
        |> Task.perform identity


counts : LiveEmojis -> List (String, Int)
counts (LiveEmojis { emojiCount }) =
    Dict.toList emojiCount

update : Msg -> LiveEmojis -> (LiveEmojis, Cmd Msg)
update msg model =
    case msg of
        AddEmoji emoji offset ->
            addEmoji emoji offset model
        AnimateEmoji index ->
            ( animateEmoji Anim index model, Cmd.none )
        HideEmoji index ->
            ( animateEmoji Dead index model, Cmd.none )
        EmojiError _ ->
            ( model, Cmd.none )


init : LiveEmojis
init =
    LiveEmojis
        { buffered = Array.empty
        , nextFree = 0
        , emojiCount = Dict.empty
        }


addEmoji : String -> Int -> LiveEmojis -> (LiveEmojis, Cmd Msg)
addEmoji newEmoji offset (LiveEmojis { buffered, nextFree, emojiCount }) =
    let
        pushOp =
            if length buffered <= nextFree then push else set nextFree

        command =
            Cmd.batch
                [ timeout 10 <| AnimateEmoji nextFree
                , timeout 2000 <| HideEmoji nextFree
                ]

        incrementEmoji =
            Just << Maybe.withDefault 1 << Maybe.map ((+) 1)

        updatedCount =
            Dict.update newEmoji incrementEmoji emojiCount
    in
        ( LiveEmojis
            { buffered = pushOp (Emoji newEmoji Init offset) buffered
            , nextFree = modBy 10 <| nextFree + 1
            , emojiCount = updatedCount
            }
        , command
        )

animateEmoji : Visibility -> Int -> LiveEmojis -> LiveEmojis
animateEmoji newState index (LiveEmojis { buffered, nextFree, emojiCount }) =
    let
        updatedBuffered =
            Array.get index buffered
                |> Maybe.map (\{ emoji, offset } ->
                    set index (Emoji emoji newState offset) buffered
                )
                |> Maybe.withDefault buffered
    in
        LiveEmojis
            { buffered = updatedBuffered
            , nextFree = nextFree
            , emojiCount = emojiCount
            }

view : LiveEmojis -> Html msg
view (LiveEmojis { buffered }) =
    let
        toHtml (index, { emoji, visible, offset }) =
            ( String.fromInt index
            , Html.div
                [ classList
                    [ ( "init", visible == Init )
                    , ( "onscreen", visible == Anim )
                    , ( "dead", visible == Dead )
                    ]
                , style "left" (String.fromInt offset ++ "%")
                , style "position" "absolute"
                ]
                (Emoji.textWith replaceWithTwemoji emoji)
            )
    in
        HK.ul
            [ class "live-emoji" ]
            (List.map toHtml <| Array.toIndexedList buffered)



sub : Sub Msg
sub =
    emojiSub (\msg ->
        case msg of
            Ok (emoji, offset) -> AddEmoji emoji offset
            Err msg_ -> EmojiError msg_
    )


emojiDecoder : Decoder (String, Int)
emojiDecoder =
    Decode.map2
        (\emoji offset -> (emoji, offset))
        (Decode.index 0 Decode.string)
        (Decode.index 1 Decode.int)


emojiSub : (Result String (String, Int) -> msg) -> Sub msg
emojiSub adapt =
    let
        fullAdapt value =
            decodeValue emojiDecoder value
                |> Result.mapError errorToString
                |> adapt
    in
        Ports.wslisten fullAdapt
