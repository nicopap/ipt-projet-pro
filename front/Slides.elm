module Slides exposing (slides, length)

import Html exposing (Html, ul, li, text, div, img, source, h1, h2, h3, p, a)
import Html.Attributes exposing (class, id, src, style, width, height)
import Array exposing (Array)

cover : Int -> Maybe (Html Never)
cover i =
    case i of
        0 -> Just <| div [ id "cover" ]
            [ h1 [ id "cover-title" ] [ text "Projet Professionnel" ]
            , p [ class "cover-subtitle" ] [ text "Nicola Papale" ]
            ]
        _ -> Nothing

title : String -> Html Never
title titleWord =
    h1 [ class "title" ] [ text titleWord ]

imgres file = "/p/p/" ++ file ++ ".png"
just id_ content = Just <| div [ id id_, class "slide" ] content
img_ source = img [ class "simple-logo", src (imgres source) ] []
largeImg_ source = img [ class "tall-image", src (imgres source) ] []
arrow = img [ class "arrow", src (imgres "arrow") ] []


-- Link to source code
sourceCode : Int -> Maybe (Html Never)
sourceCode i =
    let
        sourceLogos =
             List.map img_
                    [ "cloudflare", "nginx", "linux"
                    , "raspberry", "git", "elm"
                    , "python", "w3c"
                    ]
    in
        if i == 0 then
            just "tech-background"
                [ title "Cette Présentation"
                , p [ class "fake-link" ] [ text "nicopap.ch/p/presentation.html" ]
                , div
                    [ style "display" "flex", style "filter" "opacity(0%)" ]
                    sourceLogos
                ]
        else if i == 1 then
            just "tech-background"
                [ title "Cette Présentation"
                , p [ class "fake-link" ] [ text "nicopap.ch/p/presentation.html" ]
                , div [ style "display" "flex" ] sourceLogos
                ]
        else
            Nothing

-- URL of reaction app
-- Talk about not including tomato
-- Python + elm + WebSockets
reactions : Int -> Maybe (Html Never)
reactions i =
    if i == 0 then
        just "reactions"
            [ title "Réagissez"
            , img [ class "large-qr", src (imgres "reactqr") ] []
            , p [ class "qr-subtext", class "fake-link" ] [ text "nicopap.ch/i" ]
            ]
    else
        Nothing

-- Present various projects
--TODO: Add logo of technologies used for each project
whatIDo : Int -> Maybe (Html Never)
whatIDo i =
    let
        projectCard source =
            div [ style "width" "30%"
                , style "background-image" ("url(" ++ imgres source ++ ")")
                , class "card"
                ] [text " "]
        projectList source list =
            Just <| div [ id "projets" ]
               [ projectCard source
               , div [ class "descr" ]
                  [ title "Mes Projets"
                  , ul [ class "project-list" ] <|
                      List.map (\t -> li [] [ text t ]) list
                  ]
               ]
    in
        case i of
            0 -> projectList "none" []
            1 -> projectList "friendsketch-thumb" [ "friendsketch.com" ]
            2 -> projectList "twine-server-thumb" [ "friendsketch.com", "Twine-Server" ]
            3 -> projectList "atom-boilua-thumb" [ "friendsketch.com", "Twine-Server", "Atom-boilua" ]
            4 -> projectList "university-projects-thumb" [ "friendsketch.com", "Twine-Server", "Atom-boilua" , "Bien d'autres" ]
            5 -> Just <| div [ id "projets" ]
                [ projectCard "university-projects-thumb"
                , div [class "descr" ]
                    [ title "Mes Projets"
                    , p [ class "project-details" ]
                        [ text "Visitez "
                        , a [ class "fake-link" ] [ text "nicopap.ch/fr/porfolio" ]
                        , text " pour plus de détails"
                        ]
                    ]
                ]
            _ -> Nothing


-- What I care about
whatICareAbout : Int -> Maybe (Html Never)
whatICareAbout i =
    let
        ossList = List.map img_
            [ "gimp", "krita", "inkscape", "firefox", "scribus"
            , "libreoffice", "zim", "vim", "raspberry", "osm"
            , "gnu"
            ]
    in
        case i of
            0 -> just "passions-init" []
            1 -> just "passions" [ title "Mes passions" ]
            2 -> just "passions"
                [ title "Mes passions"
                , p [ class "passions-sub1" ] [ text "L'Open Source" ]
                ]
            3 -> just "passions"
                [ title "Mes passions"
                , p [ class "passions-sub1" ] [ text "L'Open Source" ]
                , p [ class "oss", style "display" "flex" ] ossList
                ]
            4 -> just "passions"
                [ title "Mes passions"
                , p [ class "passions-sub1" ] [ text "L'Open Source" ]
                , p [ class "oss", style "display" "flex" ] ossList
                , p [ class "passions-sub2" ] [ text "Jeux vidéos" ]
                ]
            _ -> Nothing

parcours : String -> List (Html Never) -> Int -> Maybe (Html Never)
parcours id_ list howManySteps =
    list
        |> List.take (howManySteps + 1)
        |> List.intersperse arrow
        |> (\steps -> just id_
            [ title "Mon parcours", div [ class "parcours" ] steps ]
        )

myPath : Int -> Maybe (Html Never)
myPath i =
    let pageStep =
            parcours "parcours-prev" <|
                List.map img_ ["unige", "protonmail", "ipt"]
    in
        if i < 3 && i >= 0 then pageStep i else Nothing


myPath2 : Int -> Maybe (Html Never)
myPath2 i =
    let pageStep =
            parcours "parcours-next"
                [ img_ "ipt"
                , largeImg_ "og-wsis"
                , largeImg_ "fif-azuni"
                ]
    in
        if i < 3 && i >= 0 then pageStep i else Nothing


contacts : Int -> Maybe (Html Never)
contacts i =
    let
        pageStep steps =
            just "contacts" <| List.take (steps + 1)
                [ title "Les contacts"
                , p [] [ text "Informel: ", img_ "jpmorg", img_ "esri" ]
                , p [] [ text "Entretiens: ", img_ "cimpress", img_ "loim", img_ "sonar", img_ "gyg" ]
                , p [] [ text "En réalité: bien plus" ]
                ]
    in
        if i < 4 && i >= 0 then pageStep i else Nothing


whatIearned : Int -> Maybe (Html Never)
whatIearned i =
    if i == 0 then
        just "progres"
            [ title "Les progrès"
            , ul [ class "progress-list" ] <|
                List.map (\t -> li [] [ text t ])
                    [ "Confiance en soi" , "Communication" , "Réseau" ]
            ]
    else
        Nothing


whatNext : Int -> Maybe (Html Never)
whatNext i =
    just "next"
        [ title "Et ensuite?"
        , img_ "oss"
        ]

slides : Array (Int -> Maybe (Html Never))
slides =
    Array.fromList
        [cover, sourceCode, reactions , whatIDo , whatICareAbout, myPath, myPath2, contacts, whatIearned, whatNext]

length : Int
length = Array.length slides
