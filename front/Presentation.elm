import Browser
import Html exposing (Html, text, div)
import Html.Attributes as Attr exposing (class)
import Json.Decode as Dec
import Slide exposing (Slide)
import LiveEmojis exposing (LiveEmojis)

main : Program () Model Msg
main = Browser.element
    { init = always (init, Cmd.none)
    , update = update
    , view = view
    , subscriptions = always sub
    }


sub : Sub Msg
sub =
    Sub.batch
        [ Sub.map LiveEmojisMsg LiveEmojis.sub
        , Sub.map SlideMsg Slide.sub
        ]

type alias Model =
  { slide: Slide
  , live: LiveEmojis
  }

type Msg
    = SlideMsg Slide.Msg
    | LiveEmojisMsg LiveEmojis.Msg

init : Model
init =
    { slide = Slide.init
    , live = LiveEmojis.init
    }

update : Msg -> Model -> (Model, Cmd Msg)
update msg ({ slide, live } as model) =
    case msg of
        SlideMsg msg_ ->
            ( { model | slide = Slide.update msg_ slide }, Cmd.none )
        LiveEmojisMsg msg_ ->
            let (newLive, cmds) = LiveEmojis.update msg_ live
            in ( { model | live = newLive }, Cmd.map LiveEmojisMsg cmds )


view : Model -> Html Msg
view { slide, live } =
    div []
        [ Html.map SlideMsg <| Slide.view slide <| LiveEmojis.counts live
        , Html.map LiveEmojisMsg <| LiveEmojis.view live
        ]
