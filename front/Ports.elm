port module Ports exposing (wslisten, wssend, cmdlisten)
import Json.Encode exposing (Value)

port wslisten : (Value -> msg) -> Sub msg
port cmdlisten : (Value -> msg) -> Sub msg
port wssend : String -> Cmd msg
