module Slide exposing (Slide, Msg, update, view, init, sub)

import Html exposing (Html, ul, li, text, div, h1)
import Html.Attributes exposing (class, id)
import Browser.Events as Events exposing (onKeyPress)
import Slides
import Json.Decode as Decode exposing (Decoder, decodeValue, errorToString)
import Json.Encode as Encode exposing (Value)
import Array
import Ports

keyDecoder : Decode.Decoder Msg
keyDecoder = Decode.map toMsg (Decode.field "key" Decode.string)

toMsg : String -> Msg
toMsg string =
  case string of
    "k" -> Next
    "j" -> Previous
    _ -> Step


type Slide
    = Slide
        { slide : Int
        , step : Int
        , current : Maybe (Html Never)
        }

type Msg
    = Step
    | Next
    | Previous
    | DecodeError

init : Slide
init = Slide
    { current = Maybe.andThen (\f -> f 0) (Array.get 0 Slides.slides)
    , step = 0
    , slide = 0
    }

sub : Sub Msg
sub =
    let
        fullAdapt value =
            decodeValue Decode.string value
                |> (\msg -> case msg of
                    Ok "P" -> Previous
                    Ok "S" -> Step
                    Ok "N" -> Next
                    _ -> DecodeError
                )
    in
        Sub.batch
            [ onKeyPress keyDecoder
            , Ports.cmdlisten fullAdapt
            ]


setSlide : Int -> Slide -> Slide
setSlide slide_ (Slide { slide }) =
    let
        current = Maybe.andThen (\f -> f 0) (Array.get slide_ Slides.slides)
    in
        Slide { slide = slide_, current = current, step = 0 }

nextSlide (Slide {slide} as model) =
    setSlide (min (Slides.length + 1) (slide + 1)) model

previousSlide (Slide {slide} as model) =
    setSlide (max 0 (slide - 1)) model


nextStep : Slide  -> Slide
nextStep (Slide { slide, step } as model) =
    let
        current =
            Maybe.andThen (\f -> f (step + 1)) (Array.get slide Slides.slides)
    in
        case current of
            Nothing ->
                nextSlide model
            Just _ ->
                Slide { step = step + 1, slide = slide, current = current }


update : Msg -> Slide -> Slide
update msg =
    case msg of
        Step -> nextStep
        Next -> nextSlide
        Previous -> previousSlide
        DecodeError -> identity


view : Slide -> List (String, Int) -> Html Msg
view (Slide { current }) emojis =
    let
        toEntry (emo, count) =
            li [] [ text (emo ++ ": " ++ String.fromInt count) ]
        page =
            div [ id "thanks", class "slide" ]
                [ h1 [ class "title" ] [ text "Merci" ]
                , ul [ class "emojis" ] (List.map toEntry emojis)
                ]
    in
        Maybe.map (Html.map never) current
            |> Maybe.withDefault page




